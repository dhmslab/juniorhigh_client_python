__author__ = 'rahar'
import requests
import json

#url = "http://localhost:5000"
url = "http://www.juniorhighgame.com"


def getActiveGames(state):
    headers = {
        'content-type': 'application/json',
        'authorization': 'Bearer ' + state['authToken']
    }
    r = requests.get(url + "/json/games", headers=headers)

    state['status_code'] = r.status_code
    state['isOk'] = r.status_code == requests.codes.ok
    if r.json()['res'] == "OK":
        state['games'] = r.json()['data']
    else:
        print('Err: ' + r.json()['err'])
        state['error'] = r.json()['err']
    return state


def getGameState(state):
    headers = {
        'content-type': 'application/json',
        'authorization': 'Bearer ' + state['authToken']
    }
    r = requests.get(url + "/json/games/" + str(state['gameId']) + "/state/" + str(state['round']), headers=headers)

    state['status_code'] = r.status_code
    state['isOk'] = r.status_code == requests.codes.ok
    if r.json()['res'] == "OK":
        state = dict(state.items() + r.json()['data'].items())
    else:
        print('Err: ' + r.json()['err'])
        state['error'] = r.json()['err']
    return state


def joinGame(state, gameId):
    headers = {
        'content-type': 'application/json',
        'authorization': 'Bearer ' + state['authToken']
    }
    r = requests.post(url + "/json/games/" + str(gameId) + "/join", headers=headers)

    state['status_code'] = r.status_code
    state['isOk'] = r.status_code == requests.codes.ok

    if r.json()['res'] == "OK":
        state['gameId'] = gameId
        state['round'] = 0
    else:
        state['error'] = r.json()['err']
    return state


def startGame(state, gameId):
    headers = {
        'content-type': 'application/json',
        'authorization': 'Bearer ' + state['authToken']
    }
    r = requests.post(url + "/json/games/" + str(gameId) + "/start", headers=headers)

    state['status_code'] = r.status_code
    state['isOk'] = r.status_code == requests.codes.ok

    if r.json()['res'] == "OK":
        state['gameId'] = gameId
        state['round'] = 0
    else:
        state['error'] = r.json()['err']
    return state


def createGame(state, name, maxNumberOfRounds, maxNumberOfPlayers, maxRoundTimeInSeconds):
    headers = {
        'content-type': 'application/json',
        'authorization': 'Bearer ' + state['authToken']
    }
    formData = {
        'name': name,
        'maxrounds': maxNumberOfRounds,
        'maxplayers': maxNumberOfPlayers,
        'timeout': maxRoundTimeInSeconds
    }
    r = requests.post(
        url + "/json/games"
        , data=json.dumps(formData)
        , headers=headers)

    state['status_code'] = r.status_code
    state['isOk'] = r.status_code == requests.codes.ok

    if r.json()['res'] == "OK":
        print('Game created!')
        state['gameId'] = r.json()['game']
    else:
        print('Err: ' + r.json()['err'])
        state['error'] = r.json()['err']
    return state


def makeMove(state, transactions):
    headers = {
        'content-type': 'application/json',
        'authorization': 'Bearer ' + state['authToken']
    }
    r = requests.post(
        url + "/json/games/" + str(state['gameId']) + "/transactions/" + str(state['round'])
        , data=json.dumps(transactions)
        , headers=headers)

    state['status_code'] = r.status_code
    state['isOk'] = r.status_code == requests.codes.ok

    if r.json()['res'] == "OK":
        print('Transaction successful')
    else:
        print('Err: ' + r.json()['err'])
        state['error'] = r.json()['err']
    return state