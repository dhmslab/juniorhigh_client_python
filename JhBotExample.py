import math

__author__ = 'rahar'
import JhApi as api
import time
state = {'authToken': "YOUR_TOKEN_HERE"}

state = api.getActiveGames(state)

gameId = -1

if len(state['games']) == 0:
    print("No games are available to join. Creating a new one")
    state = api.createGame(state, "It's alive!", 25, 10, 90)
    gameId = state['gameId']
else:
    gameId = state['games'][0]['id']

if gameId < 0:
    print('Unable to join the game')
    exit(1)

# GAME JOINING PART IS HERE
state = api.joinGame(state, gameId)
print(state)


# Ypu need to fill out this function.
def decideMoves(state):
    print("Making move")
    # Append you transactions to this array and return it
    transactions = []
    # This variable holds number of available tokens for this round
    availableToken = state["availableToks"]
    # Users and tokens received from them at previous round
    users = state["users"]

    #lets write simple tit-for-tat algorithm
    for user in users:
        # For each player in players of current game
        #If not current user
        if not user['isCurrentPlayer']:
            #Send the received amount back
            transactions.append({
                'dst':      user['id'],
                'amount':   user['received'] if availableToken - math.fabs(user['received']) >= 0 else 0  # if possible.
            })
            availableToken -= math.fabs(user['received'])
    return transactions

minUserTostartGame = 2
prevRound = 0
while True:
    print("Getting game state")
    newState = api.getGameState(state)
    if newState['state'] == 0:
        if len(newState["users"]) >= minUserTostartGame:
            print("Enough users have joined. Starting the game")
            newState = api.startGame(newState, gameId)

    if newState['state'] == 2:
        break

    if newState['state'] == 1 and newState['round'] > prevRound:
        # New Round
        transactions = decideMoves(newState)
        newState = api.makeMove(newState, transactions)
        prevRound = newState['round']
    state = dict(state.items() + newState.items())
    time.sleep(5)




